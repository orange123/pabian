<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pabian');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ' rYUq8!IWv0~:H.c?9IzjW&nRBfioXu^rcr26f_s*z|q*/>ZMIOt]N!W?$oPA;|0');
define('SECURE_AUTH_KEY',  'zf(k[[?AtLoK(GY_j`|f6e9,><KvpiI;bU|.4w^hSm=mRI(K-2)G-V2(Hp^>:yJh');
define('LOGGED_IN_KEY',    'G]Sq_S_x?+fRaafh+:Q4o</T3[d oT+G]~=xY$KJJ9D=+f%+%~FN;g]$C7Weot)1');
define('NONCE_KEY',        '&mu%:OYDXi!I6Tvy7Gf4n1X)=Q5Xx-NmI>O/%1.)U57;A@enZWdt6hw*B8w:4WQo');
define('AUTH_SALT',        '4n,AX0^/*.-4Xh<#MEm4aZ [w|gg]gK@dhvqX<B2$u?}P=BFwD](iv#MA{e?gnam');
define('SECURE_AUTH_SALT', '3O?-h*2S|AY7(.MJU5p&Km&2t~Ug=@vIZ-lS3v/zN!`@v3L!yEFra8ofmrj7}0QR');
define('LOGGED_IN_SALT',   'ZGDK{M:RNViZP`)`gaj/mrm?!g`cTakb[K<`jS+KAI[]$>w*T$?sBg_jIziV5D|C');
define('NONCE_SALT',       ' :=w7t!#SBBM%u[}&XCcq6vU}J}PxAKhKrnM,Ct}tj[=kWq4|}?X6D{$;L 2Sm%A');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

set_time_limit(6000);