<?php 
/* Template Name: Home Page*/


get_header(); ?>

<?php
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
		the_content();
	endwhile;
else :
	echo wpautop( 'Sorry, no posts were found' );
endif;
?>

<?php get_footer(); ?>